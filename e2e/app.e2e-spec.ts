import { NgGitPage } from './app.po';

describe('ng-git App', function() {
  let page: NgGitPage;

  beforeEach(() => {
    page = new NgGitPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
